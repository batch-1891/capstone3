import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

	//Allows us to consume the User context object and its properties to use for user validation
	const { user, setUser} = useContext(UserContext);

	//State hooks to store the values of the input fields
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	//State to determine wether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	function authenticate(e) {

		e.preventDefault();

		/*
		Syntax:
				fetch('url', {options})
				.then(res => res.json())
				.then( data => {})
		*/

		fetch('https://tranquil-woodland-71599.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
					'Content-Type': 'application/json'
			},
			body: JSON.stringify({
					email: email,
					password: password
			})
		})
		.then(res => res.json())
		.then(data => {


				/*
"access": 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyZ…yNTF9.ms9gEImY-f1UXcImqEvONxfVUHgbQAQHIGGRHHnQ6jc'
				*/

				if(typeof data.access !== "undefined") {

						localStorage.setItem('token', data.access)

						retrieveUserDetails(data.access)

						Swal.fire({
							title: "Login Successful",
							icon: "success",
							text: "Welcome to Product"
						})

				} else {

						Swal.fire({
							title: "Email or Password incorrect",
							icon: "info",
							"text": "Check your login details and try again!"
						})

				}

		})


		//Set the email of the authenticated user in the local storage
		/*
				Syntax:
						localStorage.setItem("propertyName", value)
		*/
		// localStorage.setItem("email", email);


		//Set the global user state to have properties obtained from local storage
		// setUser({
		// 		email: localStorage.getItem('email')
		// })
		

		// Clear input fields after submission

		setEmail("");
		setPassword('');

		// alert("successful login");
	}


	const retrieveUserDetails = (token) => {

				fetch('https://tranquil-woodland-71599.herokuapp.com/users/details', {
						headers: {
							Authorization: `Bearer ${token}`
						}
				})
				.then(res => res.json())
				.then(data => {

						setUser({
							id: data._id,
							isAdmin: data.isAdmin
						})

				})

	}





	useEffect(() => {

		if((email !== "" && password !== "") ) {

			setIsActive(true)
		} else {
			setIsActive(false)
		}

	}, [email,password]);

	return (

	(user.id !== null) ?
			<Navigate to="/products" />

			:

	<Form className="mt-3" onSubmit={(e)=> authenticate(e)}>
	  <h1 className="text-center">Login</h1>

      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
        		type="email"
        		placeholder="Enter email"
        		value={email}
        		onChange={e => {
        			setEmail(e.target.value)
        		}}
        		required 

        		/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control 
        		type="password" 
        		placeholder="Password"
        		value={password}
        		onChange={e => {
        			setPassword(e.target.value)
        		}}
        		required />
      </Form.Group>

      {
      	isActive ?
      		      <Button variant="success" type="submit" id="submitBtn">
        			Submit
      			  </Button>
      			  :
      			  <Button variant="danger" type="submit" id="submitBtn" disabled>
        			Submit
      			  </Button>
      }
      <p>Don't have an account? <Link to="/register" className="text-decoration-none">Sign Up</Link></p>

    </Form>

		)
}

