import { useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function CourseView() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	//The useParams hook allows us to retrieve the courseId passed via the URL
	const { courseId } = useParams()

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);


	const enroll = (courseId) => {

		fetch("https://tranquil-woodland-71599.herokuapp.com/users/order", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data) //true //false

			if (data === true) {

				Swal.fire({
					title: "Successfully Buy",
					icon: "success",
					text: "You have successfully Buy."
				})

				navigate("/products")

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}


		})
	}




	useEffect(() => {
		console.log(courseId)
		fetch(`https://tranquil-woodland-71599.herokuapp.com/products/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})


	}, [courseId])

	return(
			<Container className="mt-5">
				<Row>
					<Col lg={{span:6, offset:3}}>
						<Card>
							<Card.Body className="text-center">
								<Card.Title>{name}</Card.Title>
								<Card.Subtitle>Description:</Card.Subtitle>
								<Card.Text>{description}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>{price}</Card.Text>

						{
							user.id !== null ?
								<Button variant="primary" onClick={() => enroll(courseId)}>Buy</Button>
								:
							<Link className="btn btn-danger" to="/login">Login to Buy</Link>

						}

							</Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>
		)


}