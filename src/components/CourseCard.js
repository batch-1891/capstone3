
import {Row, Col, Card, Button, Container} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {
  
	const {name, description, price, _id} = courseProp


  return (


    <Row className="mt-3 mb-3 text-center">
      <Col xs= {12} md={6} className="mx-auto mt-4">   
      <Card className="cardHighlight p-3">
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>PhP {price}</Card.Text>
          <Button variant="primary" as={Link} to={`/products/${_id}`}>Details</Button>  
        </Card.Body>
      </Card>
      </Col>
    </Row>

    );
}





