import {Row, Col, Card, Nav} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Highlights() {

	return (

			<Row className="my-4">
				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
					<Link as={Link} to="/products">
					<img src="https://th.bing.com/th/id/R.6b544bb234a7c7fbc9baf0f68de2052b?rik=DK83indXtyIu2w&riu=http%3a%2f%2fanalyzingmarket.com%2fwp-content%2fuploads%2f2020%2f11%2fApple-foldable-phone.jpg&ehk=2flaDbUQtb%2fzrviSBConP9ZvA8JDVhds%2fyfp0BD58Z8%3d&risl=&pid=ImgRaw&r=0" height="180"/>
					</Link>
     				 <Card.Body>
       				 <Card.Title className="text-center">Apple</Card.Title>
      				 <Card.Text>
         				 The iPhone is a line of smartphones designed and marketed by Apple Inc. These devices use Apple's iOS mobile operating system.
       				 </Card.Text>
      				</Card.Body>
   				 </Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
					<Link as={Link} to="/products">
					<img src="https://www.techgenyz.com/wp-content/uploads/2021/07/Samsung-S21-Ultra-5G-1200x675.jpg" height="180"/>
					</Link>
     				 <Card.Body>
       				 <Card.Title className="text-center">Samsung</Card.Title>
      				 <Card.Text>
         				 The Samsung Galaxy S22 is a series of Android-based smartphones designed, developed, manufactured, and marketed by Samsung Electronics as part of its Galaxy S series.
       				 </Card.Text>
      				</Card.Body>
   				 </Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
					<Link as={Link} to="/products">
					<img src="https://i.pinimg.com/originals/df/37/ff/df37ff1c8e62643abc8d3da49be2a537.jpg" height="180"/>
					</Link>
     				 <Card.Body>
       				 <Card.Title className="text-center">Xiaomi</Card.Title>
      				 <Card.Text>
         				 The Xiaomi Mi 11 is an Android-based high-end smartphone designed, developed, produced, and marketed by Xiaomi Inc. succeeding their Xiaomi Mi 10 series
       				 </Card.Text>
      				</Card.Body>
   				 </Card>
				</Col>
			</Row>


		)
}