// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';
// import Container from 'react-bootstrap/Container';
// import {useState} from 'react';

import {useContext} from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import { FcTwoSmartphones } from "react-icons/fc";

export default function AppNavbar() {

    const {user} = useContext(UserContext);

    //State to store the user information stored in the login page
    // const [user, setUser] = useState(localStorage.getItem("email"));

    /*
    email: admin@mail.com
        Syntax:
            localStorage.getItem("propertyName")
        getItem() method that returns value of a specified object item

    */
    





	return (
	 <Navbar bg="info" expand="lg" sticky="top">
      <Container>
        <Navbar.Brand as={Link} to="/"><FcTwoSmartphones/></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
          <Nav>
            <Nav.Link as={Link} to="/">Home</Nav.Link>
            <Nav.Link as={Link} to="/products">Product</Nav.Link>
            </Nav>
            <Nav className="justify-content-end">
            {

            (user.id !== null) ?
            	<>
                  <Nav.Link as={Link} to="/order">Order</Nav.Link>
                  <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                </>
                  :
                  <>
                  <Nav.Link as={Link} to="/login">Login</Nav.Link>
                  <Nav.Link as={Link} to="/register">Register</Nav.Link>
                  </>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
		)
}