import { useState, useEffect, useContext } from 'react';
import { Card, Button} from 'react-bootstrap';
import { Link, useParams } from 'react-router-dom'
import UserContext from '../UserContext'


export default function OrderCard ({orderProp}) {

  const {user} = useContext(UserContext)

  const {productId} = useParams()


  const [name, setName] = useState("")
  const [description, setDescription] = useState("")
  const [price, setPrice] = useState(0)

  const order = (productId) => {
    fetch(`https://tranquil-woodland-71599.herokuapp.com/users/order`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        productId: productId,
        totalAmount: totalAmount
      })
    })

    .then(response => response.json())
    .then(data => {
      console.log(data);

      })
  }





  useEffect(() =>{
    fetch(`https://tranquil-woodland-71599.herokuapp.com/users/myOrder`)
      .then((response) => {

    });
    console.log(productId)
  })


  const { totalAmount, orderForm, purchasedOn, _id } = orderProp;
  console.log(orderProp)

  return (
          <Card>
                <Card.Body>
                  <Card.Title>{orderForm}</Card.Title>
                  <Card.Subtitle>Order Processed</Card.Subtitle>
                  <Card.Text>{purchasedOn}</Card.Text>
                  <Card.Subtitle>Price:</Card.Subtitle>
                  <Card.Text>{totalAmount}</Card.Text>
                <Button class="orderbutton" variant="primary" as={Link} to={`/products/${_id}`}>Details</Button>
             </Card.Body>
           </Card>
    )
}

