import { useState, useEffect } from 'react';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import CourseView from './pages/CourseView';
import ErrorPage from './pages/ErrorPage';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Order from './pages/Order'
import './App.css';
import { UserProvider } from './UserContext';

function App() {

//React Context is nothing but a global state to the app. It is a way to make particular data available to all the components no matter how they are nested.

  //State hook for the user state that defined here for a global scope
  //Initialized as an object with properties from the local storage
  //This will be used to store the user information and will be used for validating if a user is logged in on the app or not
  const [user, setUser] = useState({
      id: null,
      isAdmin: null
  })

  //Function for clearing the localStorage on logout
  const unsetUser = () =>{
    localStorage.clear()
  }

  useEffect(()=> {

      fetch("https://tranquil-woodland-71599.herokuapp.com/users/details", {
        headers: {
          Authorization:`Bearer ${localStorage.getItem("token")}`
        }
      })
      .then(res => res.json())
      .then(data => {

        if(typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        } else {
          setUser({
            id: null,
            isAdmin: null
          })
        }

      })
  
  }, [])



  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/products" element={<Courses />} />
            <Route path="/products/:courseId" element={<CourseView/>} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/order" element ={<Order/>}/>
            <Route path="*" element={<ErrorPage />} />
          </Routes>
        </Container>     
      </Router>
    </UserProvider>

  );
}

export default App;
